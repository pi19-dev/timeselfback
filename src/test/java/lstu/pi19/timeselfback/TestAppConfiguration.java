package lstu.pi19.timeselfback;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.test.context.TestPropertySource;

@TestConfiguration
@TestPropertySource("classpath:application.properties")
@TestPropertySource("/application–test.properties")
public class TestAppConfiguration {
}
