package lstu.pi19.timeselfback.api;

import com.mongodb.reactivestreams.client.MongoClient;
import lombok.extern.slf4j.Slf4j;
import lstu.pi19.timeselfback.TestAppConfiguration;
import lstu.pi19.timeselfback.services.AuthService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

@ExtendWith (SpringExtension.class)
@Import(TestAppConfiguration.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
@AutoConfigureWebTestClient
@ActiveProfiles ("test")
@Slf4j
class AuthApiIntegrationTest {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    AuthService authService;

    @Autowired
    MongoClient mongoClient;

    @Test
    void createUser () {

    }

    @Test
    void login () {
    }

    @Test
    void isTokenExpired () {
    }

    @Test
    void logout () {
    }

    @Test
    void exceptionHandler () {
    }
}