package lstu.pi19.timeselfback.enums;


import java.awt.*;

public enum TaskColor {
    RED(new Color(240, 0, 0)),
    ORANGE(new Color(240, 120, 0)),
    YELLOW(new Color(240, 240, 0)),
    GREEN(new Color(120, 240, 0)),
    CYAN(new Color(0, 240, 240)),
    BLUE(new Color(0, 0, 240)),
    VIOLET(new Color(120, 0, 240)),
    GRAY(new Color(120, 120, 120)),
    WHITE(new Color(240, 240, 240));

    Color color;

    TaskColor (Color color) {
        this.color = color;
    }
}
