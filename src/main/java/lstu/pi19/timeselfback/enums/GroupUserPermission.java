package lstu.pi19.timeselfback.enums;

public enum GroupUserPermission {
    READ ("read"),
    UPDATE ("update"),
    ADMIN ("admin");

    private final String value;
    GroupUserPermission (String s) {
        value = s;
    }

    public String getValue () {
        return value;
    }
}
