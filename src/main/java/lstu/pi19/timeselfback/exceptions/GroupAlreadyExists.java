package lstu.pi19.timeselfback.exceptions;

import lstu.pi19.timeselfback.exceptions.basic.AlreadyExistsException;

public class GroupAlreadyExists extends AlreadyExistsException {
    public GroupAlreadyExists () {
        super();
    }

    public GroupAlreadyExists (String message) {
        super(message);
    }
}
