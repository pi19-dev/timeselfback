package lstu.pi19.timeselfback.exceptions;

import lstu.pi19.timeselfback.exceptions.basic.AlreadyExistsException;

public class GroupUserAlreadyExistsException extends AlreadyExistsException {
    public GroupUserAlreadyExistsException () {
        super();
    }

    public GroupUserAlreadyExistsException (String message) {
        super(message);
    }
}
