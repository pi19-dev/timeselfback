package lstu.pi19.timeselfback.exceptions;

public class IncorrectCredentialsException extends RuntimeException {
    public IncorrectCredentialsException () {
    }

    public IncorrectCredentialsException (String message) {
        super(message);
    }
}
