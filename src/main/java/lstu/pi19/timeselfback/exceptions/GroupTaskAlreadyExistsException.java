package lstu.pi19.timeselfback.exceptions;

import lstu.pi19.timeselfback.exceptions.basic.AlreadyExistsException;

public class GroupTaskAlreadyExistsException extends AlreadyExistsException {
    public GroupTaskAlreadyExistsException () {
        super();
    }

    public GroupTaskAlreadyExistsException (String message) {
        super(message);
    }
}
