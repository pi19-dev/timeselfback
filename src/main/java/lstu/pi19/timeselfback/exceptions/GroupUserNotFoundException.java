package lstu.pi19.timeselfback.exceptions;

import lstu.pi19.timeselfback.exceptions.basic.NotFoundException;

public class GroupUserNotFoundException extends NotFoundException {
    public GroupUserNotFoundException () {
        super();
    }

    public GroupUserNotFoundException (String message) {
        super(message);
    }
}
