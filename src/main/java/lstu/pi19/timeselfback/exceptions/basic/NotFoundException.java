package lstu.pi19.timeselfback.exceptions.basic;

public class NotFoundException extends RuntimeException {
    public NotFoundException () {
        super();
    }

    public NotFoundException (String message) {
        super(message);
    }
}
