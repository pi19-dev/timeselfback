package lstu.pi19.timeselfback.exceptions.basic;

public class AlreadyExistsException extends RuntimeException {
    public AlreadyExistsException () {
        super();
    }

    public AlreadyExistsException (String message) {
        super(message);
    }
}
