package lstu.pi19.timeselfback.exceptions;

public class EmailAlreadyExistsException extends RuntimeException {
    public EmailAlreadyExistsException () {
        super();
    }

    public EmailAlreadyExistsException (String message) {
        super(message);
    }
}
