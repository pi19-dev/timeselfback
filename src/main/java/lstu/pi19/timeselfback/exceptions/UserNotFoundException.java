package lstu.pi19.timeselfback.exceptions;

import lstu.pi19.timeselfback.exceptions.basic.NotFoundException;

public class UserNotFoundException extends NotFoundException {
    public UserNotFoundException () {
        super();
    }

    public UserNotFoundException (String message) {
        super(message);
    }
}

