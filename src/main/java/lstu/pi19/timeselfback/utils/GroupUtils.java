package lstu.pi19.timeselfback.utils;

import lstu.pi19.timeselfback.models.Task;
import reactor.core.publisher.Mono;

public class GroupUtils {

    private GroupUtils () {
    }

    public static void convertToGroupTask (String groupId, Mono<Task> task) {
        task.map(task1 -> {
            task1.setGroupId(groupId);
            task1.setViewsCount(0);
            return task1;
        });
    }
}
