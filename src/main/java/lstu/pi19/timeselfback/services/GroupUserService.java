package lstu.pi19.timeselfback.services;

import lstu.pi19.timeselfback.enums.GroupUserPermission;
import lstu.pi19.timeselfback.exceptions.GroupUserNotFoundException;
import lstu.pi19.timeselfback.exceptions.UserNotFoundException;
import lstu.pi19.timeselfback.models.GroupUser;
import lstu.pi19.timeselfback.repositories.GroupReactiveRepository;
import lstu.pi19.timeselfback.repositories.ProfileReactiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class GroupUserService {

    @Autowired
    GroupReactiveRepository groupRepository;

    @Autowired
    UserService userService;

    @Autowired
    ProfileReactiveRepository profileRepository;

    public Mono<String> addUser (String groupId, String userId, GroupUserPermission userPermission) {
        return profileRepository.findById(userId).flatMap(profile -> {
            if (profile != null) {
                return groupRepository.addGroupUser(groupId,
                        Mono.just(new GroupUser(userId, userPermission.getValue()))).map(GroupUser::getId);
            } else {
                return Mono.error(new UserNotFoundException("User not found"));
            }
        });
    }

    public Mono<GroupUser> getUser (String groupId, String groupUserId) {
        return groupRepository.getUser(groupId, groupUserId);
    }

    public Mono<String> updateUserPermissions (String groupId, String groupUserId, Mono<String> newPermission) {
        return newPermission
                .flatMap(s -> groupRepository.updateUserPermissions(groupId, groupUserId, s)
                .map(value -> {
                    if (value) {
                        return "1";
                    } else {
                        throw new GroupUserNotFoundException("User not found");
                    }
        }));
    }

    public Mono<String> deleteUser (String groupId, String groupUserId) {
        return groupRepository.deleteUser(groupId, groupUserId)
                .map(unused -> "1");
    }
}
