package lstu.pi19.timeselfback.services;

import com.google.common.collect.Sets;
import lstu.pi19.timeselfback.enums.GroupUserPermission;
import lstu.pi19.timeselfback.exceptions.GroupAlreadyExists;
import lstu.pi19.timeselfback.exceptions.GroupNotFoundException;
import lstu.pi19.timeselfback.exceptions.TaskNotFoundException;
import lstu.pi19.timeselfback.mappers.GroupMapper;
import lstu.pi19.timeselfback.models.Group;
import lstu.pi19.timeselfback.models.GroupUser;
import lstu.pi19.timeselfback.models.Task;
import lstu.pi19.timeselfback.repositories.GroupReactiveRepository;
import lstu.pi19.timeselfback.repositories.GroupUserReactiveRepository;
import lstu.pi19.timeselfback.repositories.ProfileReactiveRepository;
import lstu.pi19.timeselfback.repositories.TaskReactiveRepository;
import lstu.pi19.timeselfback.utils.GroupUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.HashSet;

@Service
public class GroupService {
    @Autowired
    private GroupReactiveRepository groupRepository;

    @Autowired
    private TaskReactiveRepository taskRepository;

    @Autowired
    private GroupUserReactiveRepository groupUserRepository;

    @Autowired
    private ProfileReactiveRepository profileRepository;

    @Autowired
    GroupMapper groupMapper;

    public Mono<String> createGroup (String userId, Mono<Group> newGroupMono) {
        final Mono<Group> groupMono = newGroupMono
                .flatMap(newGroup -> groupRepository.findByName(newGroup.getName())
                        .flatMap(group -> (group == null) ?
                                groupRepository.save(newGroup) :
                                Mono.error(new GroupAlreadyExists(
                                        "Group with name:" + newGroup.getName() + "already exists"))
                        )
                );
        final Mono<GroupUser> groupUserMono = groupUserRepository.save(
                new GroupUser(userId, GroupUserPermission.ADMIN.getValue()));

        return groupMono.map(group -> {
            groupRepository.addGroupUser(group.getId(), groupUserMono);
            profileRepository.addGroup(userId, group.getId());
            return group.getId();
        });
    }

    public Mono<Group> getGroupById(String groupId) {
        return groupRepository.findById(groupId).map(group -> {
            if (group == null) {
                throw new GroupNotFoundException("Group with id:" + groupId + " not found");
            } else {
                return group;
            }
        });
    }

    public Mono<String> createGroupTask(String groupId, Mono<Task> task) {
        return groupRepository.findById(groupId)
                .flatMap(group -> {
                    GroupUtils.convertToGroupTask(groupId, task);
                    return task.flatMap(task1 -> taskRepository.save(task1))
                            .map(task1 -> {
                                final String taskId = task1.getId();
                                groupRepository.addTask(groupId, taskId);
                                return taskId;
                            });
                }).switchIfEmpty(Mono.defer(() ->
                        Mono.error(new GroupNotFoundException("Group with id" + groupId + "not found")))
                );
    }

    public Flux<Group> getGroups(String name) {
        final Flux<Group> groups = groupRepository.findAll();
        if (name != null) {
            return groups.filter(group -> {
                final HashSet<String> searchChunks = Sets.newHashSet(name.split(" "));
                return Arrays.stream(group.getName().split(" ")).anyMatch(searchChunks::contains);
            });
        } else {
            return groups;
        }

    }

    public Mono<Task> getGroupTaskById(String groupTaskId) {
        return taskRepository.findById(groupTaskId);
    }

    public Flux<Task> getGroupTasks (String groupId, Integer page, @Valid Integer count) {
        return taskRepository.findAllByGroupId(groupId).map(Task::addView);
    }

    public Mono<String> updateGroupTask (String groupTaskId, Mono<Task> taskMono) {
        return taskRepository.findById(groupTaskId).flatMap(task -> {
            if (task != null) {
                return taskMono.map(task1 -> {
                    task1.setId(groupTaskId);
                    taskRepository.save(task1);
                    return "1";
                });
            } else {
                throw new TaskNotFoundException("Group task with id:" + groupTaskId + " not found");
            }
        });
    }

    public Mono<String> deleteGroup(String groupId) {
        return null;
    }

    public Mono<String> deleteGroupTask(String groupTaskId) {
        return taskRepository.deleteById(groupTaskId).map(unused -> "1");
    }

    public Flux<GroupUser> getGroupUsers(Integer page, Integer count) {
        return null;
    }

    public Mono<String> updateGroup(String groupId, Mono<Group> groupMono) {
        return groupMono.flatMap(group -> groupRepository.findById(groupId)
                .map(group1 -> {
                    if (group1 != null) {
                        group.setId(groupId);
                        groupRepository.save(group1);
                        return "1";
                    } else {
                        throw new GroupNotFoundException("Group with id:" + groupId + " not found");
                    }
        }));
    }
}
