package lstu.pi19.timeselfback.services;

import lstu.pi19.timeselfback.exceptions.TaskNotFoundException;
import lstu.pi19.timeselfback.mappers.TaskMapper;
import lstu.pi19.timeselfback.models.Task;
import lstu.pi19.timeselfback.repositories.ProfileReactiveRepository;
import lstu.pi19.timeselfback.repositories.TaskReactiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Service
public class UserTaskService {

    @Autowired
    private TaskReactiveRepository taskRepository;

    @Autowired
    ProfileReactiveRepository profileRepository;

    @Autowired
    private TaskMapper taskMapper;

    public Mono<String> createTask(String userId, Mono<Task> taskMono) {
        return taskMono.flatMap(task -> taskRepository.save(task))
                .map(Task::getId)
                .map(taskId -> {
                    profileRepository.addTask(userId, taskId);
                    return taskId;
                });
    }

    public Mono<Task> getUserTaskById(String taskId) {
        return taskRepository.findById(taskId).map(task -> {
            if (task == null) {
                throw new TaskNotFoundException("User task not found");
            } else {
                return task;
            }
        });
    }

    public Flux<Task> getUserTasks(String userId, Integer page, @Valid Integer count) {
        return taskRepository.findTaskIdsFromProfileByUserId(userId);
    }

    public Mono<String> deleteUserTask(String taskId) {
        return taskRepository.deleteById(taskId).map(unused -> "1");
    }

    public Mono<String> updateTask(String taskId, Mono<Task> taskMono) {
        return taskMono.flatMap(task -> taskRepository.findById(taskId)
                .map(task1 -> {
                    if (task1 != null) {
                        task.setId(taskId);
                        taskRepository.save(task);
                        return "1";
                    } else {
                        throw new TaskNotFoundException("Task with id:" + taskId + " not found");
                    }
                })
        );
    }
}
