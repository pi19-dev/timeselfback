package lstu.pi19.timeselfback.services;

import lombok.extern.slf4j.Slf4j;
import lstu.pi19.timeselfback.exceptions.EmailAlreadyExistsException;
import lstu.pi19.timeselfback.exceptions.IncorrectCredentialsException;
import lstu.pi19.timeselfback.mappers.UserMapper;
import lstu.pi19.timeselfback.models.Profile;
import lstu.pi19.timeselfback.models.submodels.UserData;
import lstu.pi19.timeselfback.repositories.ProfileReactiveRepository;
import lstu.pi19.timeselfback.security.JWTUtil;
import lstu.pi19.timeselfback.security.PBKDF2Encoder;
import org.openapitools.model.UserCredentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

@Service
@Slf4j
public class AuthService {

    @Autowired
    PBKDF2Encoder shaEncoder;
    @Autowired
    JWTUtil jwtUtil;
    @Autowired
    ProfileReactiveRepository profileRepository;

//    UserMapper userMapper = Mappers.getMapper(UserMapper.class);
    @Autowired
    UserMapper userMapper;

    Set<String> manuallyExpiredTokens = new HashSet<>();

    public Mono<String> createUser (Mono<Profile> profileMono) {
        return profileMono.map(profile -> {
                    profile.setPassword(shaEncoder.encode(profile.getPassword()));
                    return profile;
                })
                .flatMap(profile -> profileRepository.findByUserData(profile.getUserData())
                        .flatMap(profile1 -> {
                            if (profile1 == null) {
                                return profileRepository.insert(profile)
                                        .map(Profile::getId);
                            } else {
                                return Mono.error(new EmailAlreadyExistsException("User with presented email already exists"));
                            }
                        })
                );
    }

    public Mono<String> login (UserCredentials credentials) {
        final UserData userData = new UserData();
        userData.setEmail(credentials.getEmail());
        return profileRepository.findByUserData(userData)
                .map(profile -> {
                    if (profile != null) {
                        if (shaEncoder.encode(credentials.getPassword()).equals(profile.getPassword())) {
                            return jwtUtil.generateToken(profile);
                        } else {
                            throw new IncorrectCredentialsException("Incorrect email or password");
                        }
                    } else {
                        throw new UsernameNotFoundException("Username not found");
                    }
                });
    }

    public Boolean isTokenExpired (@Valid String token) {
        return manuallyExpiredTokens.contains(shaEncoder.encode(token)) || jwtUtil.isTokenExpired(token);
    }

    public String logout (String token) {
        if (!isTokenExpired(token)) {
            manuallyExpiredTokens.add(shaEncoder.encode(token));
            return "Logout success";
        } else {
            return "Token has already expired";
        }
    }
}
