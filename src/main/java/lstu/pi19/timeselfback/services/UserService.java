package lstu.pi19.timeselfback.services;

import lstu.pi19.timeselfback.exceptions.UserNotFoundException;
import lstu.pi19.timeselfback.models.Profile;
import lstu.pi19.timeselfback.models.submodels.UserData;
import lstu.pi19.timeselfback.repositories.ProfileReactiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UserService {

    @Autowired
    private ProfileReactiveRepository profileRepository;

    public Mono<Profile> getUserByEmail(String email){
        final UserData userData = new UserData();
        userData.setEmail(email);
        return profileRepository.findByUserData(userData).map(profile -> {
            if (profile == null) {
                throw new UserNotFoundException("User with email:" + email + " not found");
            } else {
                return profile;
            }
        });
    }

    public Flux<UserData> getUsers(Integer page, Integer count){
        return profileRepository.findAllUsersData();
    }

    public Mono<String> updateUser(String userId, Mono<Profile> newUserInfoMono) {
        return newUserInfoMono.flatMap(profile -> profileRepository.findById(userId)
            .map(profile1 -> {
                if (profile1 != null) {
                    profile.setId(userId);
                    profileRepository.save(profile);
                    return "1";
                } else {
                    throw new UserNotFoundException("User with id:" + userId + " not found");
                }
            })
        );
    }

    public Mono<String> deleteUser(String userId) {
        return profileRepository.deleteById(userId).map(unused -> "1");
    }

    public Mono<Profile> getUserById(String userId) {
        return profileRepository.findById(userId);
    }
}
