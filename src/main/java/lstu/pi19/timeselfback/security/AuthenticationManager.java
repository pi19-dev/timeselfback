package lstu.pi19.timeselfback.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class AuthenticationManager implements ReactiveAuthenticationManager {

    @Autowired
    private JWTUtil jwtUtil;

    @Override
    @SuppressWarnings("unchecked")
    public Mono<Authentication> authenticate(Authentication authentication) {
        String authToken = authentication.getCredentials().toString();

        try {
//            if (jwtUtil.isTokenExpired(authToken)) {
//                return Mono.empty();
//            }
//            Claims claims = jwtUtil.getAllClaimsFromToken(authToken);
//            List<String> rolesMap = claims.get("role", List.class);
//            List<GrantedAuthority> authorities = new ArrayList<>();
//            for (String rolemap : rolesMap) {
//                authorities.add(new SimpleGrantedAuthority(rolemap));
//            }
            return Mono.just(new UsernamePasswordAuthenticationToken(null, null, null));
        } catch (Exception e) {
            return Mono.empty();
        }
    }
}
