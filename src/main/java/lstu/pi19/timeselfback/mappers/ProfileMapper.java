package lstu.pi19.timeselfback.mappers;

import lstu.pi19.timeselfback.models.Profile;
import org.mapstruct.Mapper;
import org.openapitools.model.UpdateUserRequestBody;

@Mapper
public interface ProfileMapper {
    Profile userRequestBodyToProfile(UpdateUserRequestBody userRequestBody);
}
