package lstu.pi19.timeselfback.mappers;

import lstu.pi19.timeselfback.models.Group;
import org.mapstruct.Mapper;
import org.openapitools.model.GroupInfo;
import org.openapitools.model.NewGroupInfo;

@Mapper
public abstract class GroupMapper {
    public GroupInfo fromModel (Group group) {
        return new GroupInfo()
                .name(group.getName())
                .description(group.getDescription())
                .subscribersCount((group.getUserIds() != null) ?
                        group.getUserIds().size() : 0)
                .tasksCount((group.getTaskIds() != null) ?
                        group.getTaskIds().size() : 0);
    }

    public abstract Group toModel(NewGroupInfo newGroupInfo);
}
