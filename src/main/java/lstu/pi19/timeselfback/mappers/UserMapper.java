package lstu.pi19.timeselfback.mappers;

import lstu.pi19.timeselfback.models.Profile;
import lstu.pi19.timeselfback.models.submodels.UserData;
import org.mapstruct.Mapper;
import org.openapitools.model.User;

@Mapper(uses = {User.class, Profile.class, UserData.class})
public abstract class UserMapper {

//    Пока что не работает :(
//    @Mappings({
//            @Mapping(target = "userData.firstName", source = "user.firstName"),
//            @Mapping(target = "userData.lastName", source = "user.lastName"),
//            @Mapping(target = "userData.email", source = "user.email"),
//            @Mapping(target = "password", source = "user.password")
//    })
    public Profile userToProfile (User user) {
        UserData userData = new UserData();
        userData.setFirstName(user.getFirstName());
        userData.setLastName(user.getLastName());
        userData.setEmail(user.getEmail());

        Profile profile = new Profile();
        profile.setUserData(userData);
        profile.setPassword(user.getPassword());
        return profile;
    }

    public User profileToUser(Profile profile) {
        User user = new User();
        UserData userData = profile.getUserData();
        user
                .id(profile.getId())
                .firstName(userData.getFirstName())
                .lastName(userData.getLastName())
                .email(userData.getEmail());
        return user;
    }

    public abstract User userDataToUser(UserData userData);
}
