package lstu.pi19.timeselfback.mappers;

import lstu.pi19.timeselfback.enums.GroupUserPermission;
import org.mapstruct.Mapper;
import org.openapitools.model.UserPermissions;

@Mapper
public abstract class UserPermissionsMapper {
    public abstract GroupUserPermission toModel (UserPermissions userPermissions);

    public UserPermissions toApi (String groupUserPermission) {
        return UserPermissions.fromValue(groupUserPermission);
    }

    public abstract UserPermissions toApi (GroupUserPermission groupUserPermission);
}
