package lstu.pi19.timeselfback.mappers;

import org.mapstruct.Mapper;
import org.openapitools.model.GroupUser;

@Mapper
public interface GroupUserMapper {
    public GroupUser fromModel (lstu.pi19.timeselfback.models.GroupUser groupUser);
}
