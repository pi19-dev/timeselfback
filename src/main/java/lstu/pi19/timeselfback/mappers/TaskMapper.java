package lstu.pi19.timeselfback.mappers;

import org.mapstruct.Mapper;
import org.openapitools.model.GroupTask;
import org.openapitools.model.Task;
import org.openapitools.model.TaskTimer;

@Mapper
public interface TaskMapper {
    Task fromModelToTask (lstu.pi19.timeselfback.models.Task task);
    GroupTask fromModelToGroupTask(lstu.pi19.timeselfback.models.Task task);

    lstu.pi19.timeselfback.models.Task toModel(GroupTask groupTask);
    lstu.pi19.timeselfback.models.Task toModel(Task task);
    lstu.pi19.timeselfback.models.submodels.TaskTimer toModel (TaskTimer taskTimer);
}
