package lstu.pi19.timeselfback.repositories;

import lstu.pi19.timeselfback.models.Group;
import lstu.pi19.timeselfback.models.GroupUser;
import org.openapitools.model.GroupInfo;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Configuration
public interface GroupReactiveRepository extends ReactiveMongoRepository<Group, String> {

    @Query("update({'id': ?1}, {$add: {'userIds': ?2}})")
    Mono<GroupUser> addGroupUser (String groupId, Mono<GroupUser> groupUser);

    @Query("{'id': ?1, 'users': {$elemMatch: {'id': ?2}}}")
    Mono<GroupUser> getUser (String groupId, String groupUserId);

    @Query("{'id': ?1, 'users': {$elemMatch: {'userId': ?2}}}")
    Mono<GroupUser> getUserByUserId (String groupId, String userId);

//    @Query("update({'id': ?1, 'users': {$elemMatch: {'id': ?2}}}, {$add: {'users': ?2}})")
    @Query("update({'id': ?1}, {$add: {'userIds': ?2}})")
    Mono<Boolean> updateUserPermissions (String groupId, String groupUserId, String newPermission);

//    @Query("update({'id': ?1, 'users': {$elemMatch: {'id': ?2}}}, {$pull: {'users': ?2}})")
    @Query("update({'id': ?1}, {$pull: {'userIds': ?2}})")
    Mono<Void> deleteUser (String groupId, String groupUserId);

    @Query("{ $group: {id: 'id', subscribersCount: { $count: '$users'}, tasksCount: {$count: '$tasks'}, description: '$description'}}")
    Flux<GroupInfo> groupByUsersAndTasks(); // @Aggregation

    // Сам допереть должен
    Mono<Group> findByName (String name);

    @Query("update({id: ?0}, ?1)")
    Mono<Boolean> updateById(String groupId, Mono<Group> group);

    @Query("update({'id': ?1}, {$add: {'taskIds': ?2}})")
    void addTask (String groupId, String task);
}
