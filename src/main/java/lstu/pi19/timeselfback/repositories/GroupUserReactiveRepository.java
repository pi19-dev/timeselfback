package lstu.pi19.timeselfback.repositories;

import lstu.pi19.timeselfback.models.GroupUser;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

@Configuration
public interface GroupUserReactiveRepository  extends ReactiveMongoRepository<GroupUser, String> {
}