package lstu.pi19.timeselfback.repositories;

import lstu.pi19.timeselfback.models.Profile;
import lstu.pi19.timeselfback.models.Task;
import lstu.pi19.timeselfback.models.submodels.UserData;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Configuration
public interface ProfileReactiveRepository extends ReactiveMongoRepository<Profile, String> {

//    @Query(value = "{'userData': {'email': ?0}}")
//            fields = "{id: 1, userData: 1, }")
    Mono<Profile> findByUserData(UserData userData);
//
//    @Query("{'userData.email': ?0, 'password': ?1}")
//    Mono<Long> countByEmailAndPassword (String email, String password);

    @Query("update({id: ?0}, ?1)")
    Mono<Boolean> updateById(String userId, Mono<Profile> profile);

    @Query("{}, {'userData': 1}")
    Flux<UserData> findAllUsersData();

    @Query("{'id': ?0}, {'taskIds': 1}")
    Flux<Task> findTaskIdsByUserId(String userId);

    @Query("update({'id': ?1}, {$add: {'taskIds': ?2}})")
    Mono<Void> addTask(String userId, String taskId);

    @Query("update({'id': ?1}, {$add: {'groupIds': ?2}})")
    Mono<Void> addGroup (String userId, String groupId);
}
