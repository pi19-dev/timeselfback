package lstu.pi19.timeselfback.repositories;

import lstu.pi19.timeselfback.models.Task;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Configuration
public interface TaskReactiveRepository  extends ReactiveMongoRepository<Task, String> {

    @Query("update({'id': ?1}, ?2)")
    Mono<Boolean> update(String taskId, Mono<Task> task);

    @Query("{'id': {$in: db.profiles.find({'id': ?0}, {'taskIds': 1})}}")
    Flux<Task> findTaskIdsFromProfileByUserId(String userId);

    Flux<Task> findAllByGroupId (String groupId);
}
