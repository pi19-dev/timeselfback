package lstu.pi19.timeselfback.controllers;

import lstu.pi19.timeselfback.exceptions.UserNotFoundException;
import lstu.pi19.timeselfback.mappers.ProfileMapper;
import lstu.pi19.timeselfback.mappers.TaskMapper;
import lstu.pi19.timeselfback.mappers.UserMapper;
import lstu.pi19.timeselfback.services.UserService;
import lstu.pi19.timeselfback.services.UserTaskService;
import org.openapitools.api.UserApi;
import org.openapitools.model.Task;
import org.openapitools.model.UpdateUserRequestBody;
import org.openapitools.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Controller
//@PreAuthorize ("hasRole('USER')")
public class UserController implements UserApi {
    @Autowired
    private UserService userService;
    @Autowired
    private UserTaskService userTaskService;

    @Autowired
    private TaskMapper taskMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ProfileMapper userRequestBodyMapper;

    @Override
    public Mono<ResponseEntity<User>> getUserById (String userId, ServerWebExchange exchange) {
        return userService.getUserById(userId)
                .map(userMapper::profileToUser)
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<String>> updateUser(String userId, @Valid Mono<UpdateUserRequestBody> updateUserRequestBody, ServerWebExchange exchange) {
        return userService.updateUser(
                userId,
                updateUserRequestBody.map(userRequestBodyMapper::userRequestBodyToProfile)
        ).map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<String>> deleteUser (String userId, ServerWebExchange exchange) {
        return userService.deleteUser(userId)
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<String>> createTask (String userId, @Valid Mono<Task> task, ServerWebExchange exchange) {
        return userTaskService.createTask(userId, task.map(taskMapper::toModel))
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<Flux<Task>>> getUserTasks (String userId, @Valid Integer start, @Valid Integer count, ServerWebExchange exchange) {
        return Mono.just(new ResponseEntity<>(
                userTaskService.getUserTasks(userId, start, count)
                        .map(taskMapper::fromModelToTask),
                HttpStatus.OK)
        );
    }

    @Override
    public Mono<ResponseEntity<String>> updateTask (String taskId, @Valid Mono<Task> task, ServerWebExchange exchange) {
        return userTaskService.updateTask(taskId, task.map(taskMapper::toModel))
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<String>> deleteTask (String taskId, ServerWebExchange exchange) {
        return userTaskService.deleteUserTask(taskId)
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<Task>> getTaskById (String taskId, ServerWebExchange exchange) {
        return userTaskService.getUserTaskById(taskId)
                .map(taskMapper::fromModelToTask)
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<Flux<User>>> getUsers (@Valid Integer start, @Valid Integer count, ServerWebExchange exchange) {
        return Mono.just(new ResponseEntity<>(
                userService.getUsers(start, count).map(userMapper::userDataToUser),
                HttpStatus.OK)
        );
    }

    @ExceptionHandler
    public Mono<ResponseEntity<String>> exceptionHandler(Throwable e){
        if (e instanceof UserNotFoundException){
            return Mono.just(new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST));
        }
        else {
            return Mono.just(new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST));
        }
    }
}
