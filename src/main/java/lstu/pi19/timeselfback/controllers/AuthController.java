package lstu.pi19.timeselfback.controllers;

import lombok.extern.slf4j.Slf4j;
import lstu.pi19.timeselfback.exceptions.EmailAlreadyExistsException;
import lstu.pi19.timeselfback.exceptions.IncorrectCredentialsException;
import lstu.pi19.timeselfback.mappers.UserMapper;
import lstu.pi19.timeselfback.services.AuthService;
import org.openapitools.api.AuthApi;
import org.openapitools.model.LogoutRequest;
import org.openapitools.model.User;
import org.openapitools.model.UserCredentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Slf4j
@RestController
public class AuthController implements AuthApi {

    @Autowired
    AuthService authService;

    @Autowired
    UserMapper userMapper;

    @Override
    public Mono<ResponseEntity<String>> createUser (@Valid Mono<User> user, ServerWebExchange exchange) {
        return authService.createUser(user.map(userMapper::userToProfile))
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<String>> login (@Valid Mono<UserCredentials> userCredentials, ServerWebExchange exchange) {
        return userCredentials
                .flatMap(authService::login)
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<String>> logout (Mono<LogoutRequest> logoutRequest, ServerWebExchange exchange) {
        return logoutRequest.map(logoutRequest1 -> authService.logout(logoutRequest1.getToken()))
                .map(ResponseEntity::ok);
    }

    @ExceptionHandler
    public Mono<ResponseEntity<String>> exceptionHandler(Throwable e) {
        HttpStatus responseStatus;
        log.error(e.getMessage());
        if (e instanceof EmailAlreadyExistsException) {
            responseStatus = HttpStatus.BAD_REQUEST;
        } else if (e instanceof IncorrectCredentialsException) {
            responseStatus = HttpStatus.UNAUTHORIZED;
        } else {
            responseStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return Mono.just(new ResponseEntity<>(e.getMessage(), responseStatus));
    }
}
