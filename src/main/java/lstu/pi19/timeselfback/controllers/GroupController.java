package lstu.pi19.timeselfback.controllers;

import lombok.extern.slf4j.Slf4j;
import lstu.pi19.timeselfback.enums.GroupUserPermission;
import lstu.pi19.timeselfback.exceptions.basic.AlreadyExistsException;
import lstu.pi19.timeselfback.exceptions.basic.NotFoundException;
import lstu.pi19.timeselfback.mappers.GroupMapper;
import lstu.pi19.timeselfback.mappers.GroupUserMapper;
import lstu.pi19.timeselfback.mappers.TaskMapper;
import lstu.pi19.timeselfback.mappers.UserPermissionsMapper;
import lstu.pi19.timeselfback.services.GroupService;
import lstu.pi19.timeselfback.services.GroupUserService;
import org.openapitools.api.GroupApi;
import org.openapitools.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Slf4j
@Controller
//@PreAuthorize ("hasRole('USER')")
public class GroupController implements GroupApi {

    @Autowired
    private GroupService groupService;

    @Autowired
    GroupUserService groupUserService;

    @Autowired
    UserPermissionsMapper userPermissionsMapper;

    @Autowired
    TaskMapper taskMapper;

    @Autowired
    GroupMapper groupMapper;

    @Autowired
    GroupUserMapper groupUserMapper;

    @Override
    public Mono<ResponseEntity<String>> createGroup (String userId, Mono<NewGroupInfo> newGroupInfo, ServerWebExchange exchange) {
        return groupService.createGroup(userId, newGroupInfo.map(groupMapper::toModel))
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<Flux<GroupInfo>>> getGroups (String name, Integer page, Integer count, ServerWebExchange exchange) {
        return Mono.just(new ResponseEntity<>(
                groupService.getGroups(name)
                    .map(groupMapper::fromModel),
                HttpStatus.OK));
    }

    @Override
    public Mono<ResponseEntity<GroupInfo>> getGroup(String groupId, ServerWebExchange exchange) {
        return groupService.getGroupById(groupId)
                .map(groupMapper::fromModel)
                .map(groupInfo -> new ResponseEntity<>(groupInfo, HttpStatus.OK));
    }

    @Override
    public Mono<ResponseEntity<String>> updateGroup (String groupId, Mono<NewGroupInfo> newGroupInfo, ServerWebExchange exchange) {
        return groupService.updateGroup(groupId, newGroupInfo.map(groupMapper::toModel))
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<String>> deleteGroup(String groupId, ServerWebExchange exchange) {
        return groupService.deleteGroup(groupId).map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<String>> addUser (String groupId, String userId, @Valid UserPermissions userPermissions, ServerWebExchange exchange) {
        if (userPermissions != null)
            return groupUserService.addUser(groupId, userId, userPermissionsMapper.toModel(userPermissions))
                    .map(ResponseEntity::ok);
        else
            return groupUserService.addUser(groupId, userId, GroupUserPermission.READ)
                    .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<String>> deleteGroupUser (String groupId, String groupUserId, ServerWebExchange exchange) {
        return groupUserService.deleteUser(groupId, groupUserId)
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<String>> createGroupTask(String groupId, @Valid Mono<Task> task, ServerWebExchange exchange) {
        return groupService.createGroupTask(groupId, task.map(taskMapper::toModel))
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<String>> deleteGroupTaskById(String groupId, String groupTaskId, ServerWebExchange exchange) {
        return groupService.deleteGroupTask(groupTaskId)
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<GroupTask>> getGroupTaskById (String groupId, String groupTaskId, ServerWebExchange exchange) {
        return groupService.getGroupTaskById(groupTaskId)
                .map(taskMapper::fromModelToGroupTask)
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<Flux<GroupTask>>> getGroupTasks (String groupId, @Valid Integer page, @Valid Integer count, ServerWebExchange exchange) {
        return Mono.just(new ResponseEntity<>(
                groupService.getGroupTasks(groupId, page, count)
                    .map(taskMapper::fromModelToGroupTask),
                HttpStatus.OK)
        );
    }

    @Override
    public Mono<ResponseEntity<Flux<GroupUser>>> getGroupUsers(String groupId, @Valid Integer page, @Valid Integer count, ServerWebExchange exchange) {
        return Mono.just(new ResponseEntity<>(
                groupService.getGroupUsers(page, count)
                        .map(groupUserMapper::fromModel),
                HttpStatus.OK));
    }

    @Override
    public Mono<ResponseEntity<UserPermissions>> getUserPermissions (String groupId, String groupUserId, ServerWebExchange exchange) {
        return groupUserService.getUser(groupId, groupUserId)
                .map(lstu.pi19.timeselfback.models.GroupUser::getPermission)
                .map(userPermissionsMapper::toApi)
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<String>> updateGroupTaskById (String groupId, String groupTaskId, Mono<GroupTask> groupTask, ServerWebExchange exchange) {
        return groupService.updateGroupTask(
                groupTaskId,
                groupTask
                    .map(taskMapper::toModel))
                .map(ResponseEntity::ok);
    }

    @Override
    public Mono<ResponseEntity<String>> updateGroupUser (String groupId, String groupUserId, @NotNull @Valid UserPermissions userPermissions, ServerWebExchange exchange) {
        return null;
    }
    // TSBACK-13

    @Override
    public Mono<ResponseEntity<String>> updateUserPermissions (String groupId, String groupUserId, @Valid Mono<String> body, ServerWebExchange exchange) {
        return groupUserService.updateUserPermissions(
                    groupId,
                    groupUserId,
                    body
                ).map(ResponseEntity::ok);
    }

    @ExceptionHandler
    public Mono<ResponseEntity<String>> exceptionHandler(Throwable e) {
        HttpStatus responseStatus;
        log.error(e.getMessage());
        if (e instanceof AlreadyExistsException) {
            responseStatus = HttpStatus.BAD_REQUEST;
        } else if (e instanceof NotFoundException) {
            responseStatus = HttpStatus.NOT_FOUND;
        } else {
            responseStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return Mono.just(new ResponseEntity<>(e.getMessage(), responseStatus));
    }
}
