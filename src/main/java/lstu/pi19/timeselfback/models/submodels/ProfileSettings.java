package lstu.pi19.timeselfback.models.submodels;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lstu.pi19.timeselfback.enums.ColorTheme;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProfileSettings {
    @NotNull
    private Boolean notifications;
    //private Map<String, TaskColor> taskColor; // taskId: Color
    private ColorTheme colorsTheme;
}
