package lstu.pi19.timeselfback.models.submodels;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserData {
    private String firstName;
    private String lastName;
    @NotNull
    private String email;
}

