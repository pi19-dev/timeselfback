package lstu.pi19.timeselfback.models.submodels;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TaskTimer {
    private long startDate;
    @NotNull
    private long endDate;
    private int repeatBy;
}
