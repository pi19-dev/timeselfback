package lstu.pi19.timeselfback.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lstu.pi19.timeselfback.models.submodels.ProfileSettings;
import lstu.pi19.timeselfback.models.submodels.UserData;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Document (value = "profiles")
public class Profile {
    @Id
    private String id;
    @NotNull
    private UserData userData;
    @NotNull
    private String password;
    @NotNull
    private ProfileSettings settings;
    @NotNull
    private List<String> taskIds = new ArrayList<>();
    private List<String> groupTaskIds;
    private List<String> groupIds;
}