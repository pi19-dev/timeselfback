package lstu.pi19.timeselfback.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lstu.pi19.timeselfback.models.submodels.Address;
import lstu.pi19.timeselfback.models.submodels.RatingCounter;
import lstu.pi19.timeselfback.models.submodels.TaskTimer;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Document (collection = "tasks")
public class Task {
    @Id
    private String id;
    @NotNull
    private String name;
    private String description;
    private TaskTimer taskTimer;
    @NotNull
    private Boolean isActive = true;
    private Address address;

    // NotNull для группового таска
    private String groupId;
    // NotNull для группового таска
    private Integer viewsCount;
    private RatingCounter ratingCounter;

    public Task addView() {
        viewsCount++;
        return this;
    }
}
