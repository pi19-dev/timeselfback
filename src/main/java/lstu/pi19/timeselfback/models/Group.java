package lstu.pi19.timeselfback.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Document (value = "groups")
public class Group {
    @Id
    private String id;
    @NotNull
    private String name;
    private String description;
    @NotNull
    private List<String> userIds = new ArrayList<>();
    private List<String> taskIds;
    @NotNull
    private Boolean isPrivate = false;
    @NotNull
    private Boolean isDeleted = false;
}
