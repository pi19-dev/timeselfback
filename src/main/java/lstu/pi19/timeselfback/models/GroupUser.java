package lstu.pi19.timeselfback.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Document(collection = "group_users")
public class GroupUser {
    @Id
    private String id;
    @NotNull
    private String userId;
    @NotNull
    private String permission; // From GroupUserPermission

    public GroupUser (String userId, String permission) {
        this.userId = userId;
        this.permission = permission;
    }
}
