package lstu.pi19.timeselfback.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lstu.pi19.timeselfback.models.submodels.RatingCounter;
import org.springframework.data.mongodb.core.mapping.Document;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Document (collection = "group_tasks")
public class GroupTask {

    private String groupId;

    private Integer viewsCount = 0;
    private RatingCounter ratingCounter;
}
