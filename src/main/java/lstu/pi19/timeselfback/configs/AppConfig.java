package lstu.pi19.timeselfback.configs;

import lstu.pi19.timeselfback.mappers.*;
import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("lstu.pi19.timeselfback")
@PropertySource("classpath:application.properties")
@PropertySource("classpath:application-prod.properties")
public class AppConfig {

    @Bean
    public UserMapper userMapper(){
        return Mappers.getMapper(UserMapper.class);
    }
    @Bean
    public TaskMapper taskMapper(){
        return Mappers.getMapper(TaskMapper.class);
    }

    @Bean
    public UserPermissionsMapper userPermissionsMapper() {
        return Mappers.getMapper(UserPermissionsMapper.class);
    }

    @Bean
    public GroupMapper groupMapper() {
        return Mappers.getMapper(GroupMapper.class);
    }

    @Bean
    public GroupUserMapper groupUserMapper() {
        return Mappers.getMapper(GroupUserMapper.class);
    }
}
