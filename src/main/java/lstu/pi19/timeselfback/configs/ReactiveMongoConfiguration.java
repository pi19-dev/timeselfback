package lstu.pi19.timeselfback.configs;

import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;


// TODO мб не нужно
@Configuration
@EnableReactiveMongoRepositories("lstu.pi19.timeselfback.repositories")
class ReactiveMongoConfiguration extends AbstractReactiveMongoConfiguration {

    @Override
    protected String getDatabaseName () {
        return "prod";
    }

    @Override
    public MongoClient reactiveMongoClient () {
        return MongoClients.create();
    }
}